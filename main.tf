provider "google" {
  project = "spiritual-grin-232907"
  region  = "us-central1"
  zone    = "us-central1-c"
}

# Описание haproxy ВМ
resource "google_compute_instance" "haproxy" {
  name         = "haproxy"
  machine_type = "f1-micro"

  boot_disk {
    initialize_params {
      image = "centos-7"
    }
  }

  network_interface {
    # A default network is created for all GCP projects
    network       = "default"
    access_config = {
    }
  }
}

resource "google_compute_instance" "log" {
  name         = "log"
  machine_type = "n1-standard-1"

  boot_disk {
    initialize_params {
      image = "centos-7"
    }
  }

  network_interface {
    # A default network is created for all GCP projects
    network       = "default"
    access_config = {
    }
  }
}


resource "google_compute_instance" "zabbix" {
  name         = "zabbix"
  machine_type = "n1-standard-1"

  boot_disk {
    initialize_params {
      image = "centos-7"
    }
  }

  network_interface {
    # A default network is created for all GCP projects
    network       = "default"
    access_config = {
    }
  }
}

resource "google_compute_instance" "nginx01" {
  name         = "nginx01"
  machine_type = "f1-micro"

  boot_disk {
    initialize_params {
      image = "centos-7"
    }
  }

  network_interface {
    # A default network is created for all GCP projects
    network       = "default"
    access_config = {
    }
  }
}

resource "google_compute_instance" "nginx02" {
  name         = "nginx02"
  machine_type = "f1-micro"

  boot_disk {
    initialize_params {
      image = "centos-7"
    }
  }

  network_interface {
    # A default network is created for all GCP projects
    network       = "default"
    access_config = {
    }
  }
}

resource "google_compute_instance" "gitlab" {
  name         = "gitlab"
  machine_type = "f1-micro"

  boot_disk {
    initialize_params {
      image = "centos-7"
    }
  }

    network_interface {
    # A default network is created for all GCP projects
    network       = "default"
    access_config = {
    }
  }
}

resource "google_compute_instance" "backup" {
  name         = "backup"
  machine_type = "f1-micro"

  boot_disk {
    initialize_params {
      image = "centos-7"
    }
  }

  network_interface {
    # A default network is created for all GCP projects
    network       = "default"
    access_config = {
    }
  }
}

resource "google_compute_instance" "mysql" {
  name         = "mysql"
  machine_type = "f1-micro"

  boot_disk {
    initialize_params {
      image = "centos-7"
    }
  }

  network_interface {
    # A default network is created for all GCP projects
    network       = "default"
    access_config = {
    }
  }
}

resource "google_compute_instance" "pg01" {
  name         = "pg01"
  machine_type = "f1-micro"

  boot_disk {
    initialize_params {
      image = "centos-7"
    }
  }

  network_interface {
    # A default network is created for all GCP projects
    network       = "default"
    access_config = {
    }
  }
}

resource "google_compute_instance" "pg02" {
  name         = "pg02"
  machine_type = "f1-micro"

  boot_disk {
    initialize_params {
      image = "centos-7"
    }
  }

  network_interface {
    # A default network is created for all GCP projects
    network       = "default"
    access_config = {
    }
  }
}

resource "google_compute_instance" "pg03" {
  name         = "pg03"
  machine_type = "f1-micro"

  boot_disk {
    initialize_params {
      image = "centos-7"
    }
  }

  network_interface {
    # A default network is created for all GCP projects
    network       = "default"
    access_config = {
    }
  }
}

resource "google_compute_instance" "etcd" {
  name         = "etcd"
  machine_type = "f1-micro"

  boot_disk {
    initialize_params {
      image = "centos-7"
    }
  }

  network_interface {
    # A default network is created for all GCP projects
    network       = "default"
    access_config = {
    }
  }
}

resource "google_compute_network" "vpc_network" {
  name                    = "terraform-network"
  auto_create_subnetworks = "true"
}

resource "google_compute_firewall" "default" {
 name    = "nginx-app"
 network = "default"

 allow {
   protocol = "tcp"
   ports    = ["80", "3306", "8080", "19999", "10050", "10051", "5601", "5044", "9200","9300","514","8008","2379","2380","5342","7000","5000"]
 }
}
